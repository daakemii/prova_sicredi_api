package sicredi;

import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

public class SicrediApiTest {

	String BASE_URL = "https://viacep.com.br/ws"; 
		
	@Test
	public void ConsultarCepValido() {

		given()
			.contentType("application/json")
		.when()
			.get(BASE_URL + "/91060900/json/")
		.then()
			.statusCode(200)
			.body("cep", equalTo("91060-900"))
			.body("logradouro", equalTo("Avenida Assis Brasil 3940"))
			.body("complemento", equalTo(""))
			.body("bairro", equalTo("S�o Sebasti�o"))
			.body("localidade", equalTo("Porto Alegre"))
			.body("uf", equalTo("RS"))
			.body("ibge", equalTo("4314902"));
	}
	
	
	@Test
	public void ConsultarCepinexistente() {
	
		given()
			.contentType("application/json")
		.when()
			.get(BASE_URL + "/91000000/json/")
		.then()
			.statusCode(200)
			.body("erro", equalTo(true));
	}
	
	
	@Test
	public void ConsultarCepInvalido() {
		
		given()
			.contentType("application/json")
		.when()
			.get(BASE_URL + "/9100a000/json/")
		.then()
			.statusCode(400);
	}
	
	@Test
	public void ConsultarInformacoesCep() {
		given()
			.contentType("application/json")
		.when()
			.get(BASE_URL + "/RS/Gravatai/Barroso/json/")
		.then()
			.statusCode(200)
			.body("cep[0]", equalTo("94085-170"))
			.body("logradouro[0]", equalTo("Rua Ari Barroso"))
			.body("complemento[0]", equalTo(""))
			.body("bairro[0]", equalTo("Morada do Vale I"))
			.body("localidade[0]", equalTo("Gravata�"))
			.body("uf[0]", equalTo("RS"))
			.body("ibge[0]", equalTo("4309209"))
			
			.body("cep[1]", equalTo("94175-000"))
			.body("logradouro[1]", equalTo("Rua Almirante Barroso"))
			.body("complemento[1]", equalTo(""))
			.body("bairro[1]", equalTo("Recanto Corcunda"))
			.body("localidade[1]", equalTo("Gravata�"))
			.body("uf[1]", equalTo("RS"))
			.body("ibge[1]", equalTo("4309209"));
	}
	
}
